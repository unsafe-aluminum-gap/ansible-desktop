# Ansible Desktop

My personal workstation setup. I use this Ansible playbook to quickly configure any machine to suit my needs, no matter if it's for testing purposes, rolling back from some sort of error or simply trying out a new flavor of GNU/Linux.

This Ansible playbook has been tested on:

- Debian 12 (Bookworm)

## Setup Script Installation

You can run the setup script to handle all the necessary setup to run this playbook, including installing required dependencies and creating a virtual Python environment:

```sh
wget -O - -q https://codeberg.org/D10f/ansible-desktop/raw/branch/main/setup.sh | sh -
```

Once it's done, follow the instructions found in the [How To Use](#how-to-use) section.

If you'd prefer to not use a virtual Python environment and to install Ansible globally, [download this repository first](#manual-installation), and run the setup script providing the `-g` flag:

```sh
./ansible-desktop/setup.sh -g
```

## Manual Installation

If `git` is already installed on your machine you can clone this repository:

```sh
git clone https://codeberg.org/d10f/ansible-desktop.git
```

Alternatively, you can use `wget` to download an archive file, and extract its contents with `tar`:

```sh
wget -q https://codeberg.org/d10f/ansible-desktop/archive/main.tar.gz
tar -xvf main.tar.gz
```

Once that's done, make sure to install the following packages needed to run this playbook:

- python (>= 3.8)
- python3-debian (>= 0.1.49)
- ansible (>= 2.15.1)

## How To Use

Most of the roles are responsible for installing something, like Brave Browser or Docker; others, you should tune to your needs. To do that go to their respective _vars_ directory and uncomment the variables that you'd like to use with whatever values you see fit. Each role has a short "readme" file describing the purpose and meaning of the values used in it.

One of the most interesting roles that you will definitely want to customize is the _dotfiles_ role. It assumes that an online git repository exists, and attempts to download it and run an installation script found at the root level. By default it points to [my dotfiles repository](https://codeberg.org/d10f/dotfiles).

### Run It

In its most basic form you should run it with:

```sh
ansible-playbook ansible-desktop/local.yml -K
```

### Update

TODO: ansible-pull

## Credits

TODO
