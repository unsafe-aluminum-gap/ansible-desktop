# Btop

Installs Btop and downloads a few themes for it.

## Role Variables

- **force_download**: whether or not to overwrite any existing themes. Leave as false by default, only change when updates are needed.

- **theme_urls**: list of urls to download containing the styles for the Btop theme.

## License

BSD

## Author Information

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
