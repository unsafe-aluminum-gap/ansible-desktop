---
# tasks file for roles/composer

- name: Install PHP {{ php_version }}
  ansible.builtin.apt:
    name: "php{{ php_version }}-cli"
    state: present
    update_cache: True

- name: Check if Composer binary already exists
  ansible.builtin.stat:
    path: "{{ composer_bin }}"
    get_mime: False
    get_checksum: False
    get_attributes: False
  register: composer_stats

- name: Download and install composer
  when: not composer_stats.exists
  block:
    - name: Download Composer's signature
      ansible.builtin.uri:
        url: "{{ composer_signature_url }}"
        return_content: True
      register: composer_installer_signature

    - name: Download Composer installer
      ansible.builtin.get_url:
        url: "{{ composer_installer_url }}"
        dest: "{{ composer_installer_dir }}"
        mode: 0750
        checksum: "sha384:{{ composer_installer_signature.content }}"

    - name: Run Composer installer
      ansible.builtin.command:
        command: >
          /usr/bin/env php {{ composer_installer_dir }} \
          --quiet \
          --filename=composer \
          --install-dir={{ composer_bin }}

    - name: Update ownership and permissions of Composer binary
      ansible.builtin.file:
        name: "{{ composer_bin }}"
        owner: "{{ default_username }}"
        group: "{{ default_username }}"
        mode: 0700

    - name: Remove Composer installer
      ansible.builtin.file:
        name: "{{ composer_installer_dir }}"
        state: absent
