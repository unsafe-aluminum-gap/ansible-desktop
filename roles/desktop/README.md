# Desktop

Installs and configures the desktop environment, including the shell theme, fonts, icons, wallpapers, extensions, etc.

## Dependencies

- **gconf2**: for gsettings
- **gnome-shell-extensions**: to handle extensions

## Role Variables

- **current_theme**: which theme to activate.

- **catppuccin_gtk_url**: URL to the release page for the Catppuccin theme.

- **catppuccin_flavor**: One of "Latte", "Frappe", "Mocha" or "Macchiato".

- **catppuccin_themes**: List of colors from the Catppuccin palette to download.

- **home_dirs**: List of extra directories to create on the user's home directory.

- **required_packages**: List of packages to install to work with gnome (gnome only).

- **shell_extensions**: List of shell extensions to install (gnome only).

## Example Playbook

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

## License

BSD

## Author Information

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
