# Docker

Installs Docker and Docker Compose.

## Role Variables

- docker_gpg_url: GPG key for the Docker repository.

- docker_repo_url: URL for the Docker repository.

- docker_prerequisites: Required packages used by Docker and Docker Compose.

- docker_packages: Docker Engine and Docker Compose packages.

## License

BSD

## Author Information

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
