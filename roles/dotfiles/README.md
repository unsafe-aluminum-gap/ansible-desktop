# dotfiles

Downloads a git repository (presumably containing dotfiles) and attempts to run an installation script found at the root level.

## Requirements

- git

## Role Variables

- git_user_alias: the username of the author of the target dotfiles repository.

- git_repo_url: the URL used to clone the repository.

- private_ssh_key: path to the private key to use when downloading the repository using SSH.

- git_branch: the repository branch to download.

- dotfiles_location: path on the host where the repository is downloaded to.

- install_script_name: name of the installation script to run from the dotfiles repository.

- run_installation_script: whether to run or not the installation script.

- run_installation_as_root: whether to run as the root user or the default user.

## License

BSD
