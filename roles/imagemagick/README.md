# Role Name

Installs ImageMagick by building from source code.

## Role Variables

- **git_repo**: the git repository to download ImageMagick source code.
- **compile_opencl_sdk**: boolean value to compile using OpenCL.
- **image_libs**: image manipulation libraries used by ImageMagick.
- **compression_libs**: compression libraries used by ImageMagick.
- **font_libs**: font libraries used by ImageMagick.
- **utility_libs**: various utilities designed to improve efficiency and performance.
- **build_options**: arguments provided to the autotools script.

## License

BSD

## Author Information

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
