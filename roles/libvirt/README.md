# Libvirt

Installs libvirt and all auxiliary libraries to run KVM/QEMU virtual machines.

## Role Variables

- **required_packages**: package list of all required dependencies.

## License

BSD

## Author Information

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
