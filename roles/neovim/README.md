# Neovim

Installs Neovim, built from source on Debian.

## Role Variables

- **build_dependencies**: the various packages that are required by Neovim to build from source.

## License

BSD

## Author Information

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
