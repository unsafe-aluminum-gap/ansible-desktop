# Nerd Fonts

Downloads a selected list of Nerd Fonts.

## Role Variables

- **base_url**: URL to download the fonts from.
- **archive_type**: One of 'zip' or 'tar.xz' (options available for download).
- **fonts**: the list of fonts to download. Just the name, without the 'Nerd Font' suffix.

## License

BSD

## Author Information

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
