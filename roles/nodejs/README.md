# Node.js

Installs Node.js and configures NPM to install global packages under a user-controlled location.

## Role Variables

- **nodejs_version**: the node.js version to install.

- **nodejs_gpg_url**: the URL to the GPG signing key.

- **nodejs_repo_url**: the URL to the repository source.

- **npm_global_install_path**: path to the location where npm will install global packages.

## Example Playbook

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

## License

BSD

## Author Information

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
