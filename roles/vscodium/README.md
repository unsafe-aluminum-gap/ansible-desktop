# VSCodium

Installs VSCodium and several extensions.

## Role Variables

- **codium_gpg_url**: the GPG signing key URL.
- **codium_repo_url**: the repository URL.
- **vscodium_extensions**: extension list to install.

## License

BSD

## Author Information

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
