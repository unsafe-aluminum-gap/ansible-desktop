#!/bin/sh

# Installs and configures the system to run Ansible.
#
# TODO:
#   - [ ] Port to other Linux distributions / package managers.
#   - [ ] Error handling

LOCAL_BINARIES_DIR="$HOME/.local/bin"
ANSIBLE_PYVENV_DIR="$LOCAL_BINARIES_DIR/ansible"

install_ansible_globally=false
verbose=true

__help() {
cat << EOF
Usage: ${0##*/}

    Installs and configures the system to run this Ansible playbook.

Command Options:
    -g      Installs Ansible globally (with --break-system-packages).
    -h      Prints this text and exit.
    -q      Supress output.
EOF
}

__done() {
cat << EOF
All Done! Follow the instructions found in the README to learn how to
customize and run this playbook.
EOF
}

__print() {
    if [ $verbose = true ]; then
        printf "%s\n" "$1"
    fi
}

if [ "$(id -u)" -eq 0 ]; then
    echo >&2 "${0##*/}: Err: You must not run this script as root."
    exit 1
fi

while getopts "g:hq" opt; do
    case $opt in
        g) install_ansible_globally=true ;;
        h) __help; exit 0 ;;
        q) verbose=false ;;
        *)
            echo "Invalid option: $OPTARG"
            __help
            exit 1
            ;;
    esac
done

__print "Installing required packages."
sudo apt install -y python3-pip

__print "Installing Ansible..."

if [ "$install_ansible_globally" = false ]; then

    __print "Initializing virtual Python environment..."
    sudo apt install -y python3-venv
    mkdir -p "$LOCAL_BINARIES_DIR"
    python3 -m venv "$ANSIBLE_PYVENV_DIR"
    find "$ANSIBLE_PYVENV_DIR" -type d -exec chmod 750 {} \;

    (
        #shellcheck source=/dev/null
        . "$ANSIBLE_PYVENV_DIR/bin/activate"
        pip install ansible python-debian
    )
else
    sudo apt install -y python3-debian
    pip install --break-system-packages ansible
fi

ansible_install_path=$(which ansible || printf %s "$ANSIBLE_PYVENV_DIR")
__print "Ansible successfully installed in $ansible_install_path"

# __print "Downloading git repository: $ANSIBLE_PLAYBOOK_GIT_REPO"
# git clone $ANSIBLE_PLAYBOOK_GIT_REPO

__print "Downloading git repository:"
wget -q https://codeberg.org/D10f/ansible-desktop/archive/main.tar.gz
tar -xvf main.tar.gz && rm main.tar.gz

# if [ "$(tar -xvf main.tar.gz)" ]; then
#     rm main.tar.gz
# fi
__done
